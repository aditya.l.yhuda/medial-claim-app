import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Button, Modal, InputGroup, Form, Table, Row, Col } from 'react-bootstrap';
import ClaimList from './components/ClaimList';

function App() {
  return (
    <Router>
    <div className="App">
      <header className="App-header">
      <Routes>
          <Route path="/" element={<ClaimList />} />
        </Routes>
      </header>
    </div>
    </Router>
  );
}

export default App;
