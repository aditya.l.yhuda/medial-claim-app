import React, { useState, useEffect, Fragment } from "react";
import axios from 'axios';
import { Nav, Button, Modal, InputGroup, Form, Table, Row, Col } from 'react-bootstrap';

const ClaimList = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [data, setData] = useState([]);

  const [id, setId] = useState(0);
  const [patientName, setPatientName] = useState("");
  const [dateOfService, setDateOfService] = useState(new Date());
  const [medicalProvide, setMedicalProvide] = useState("");
  const [diagnosis, setDiagnosis] = useState("");
  const [claimAmount, setClaimAmount] = useState(0);
  const [status, setStatus] = useState("");

  useEffect(() => {
    getData();
  }, []);

  const handleClear = () => {
    setId(0);
    setPatientName('');
    setDateOfService('');
    setMedicalProvide('');
    setDiagnosis('');
    setClaimAmount('');
    setStatus('');
  }

  const handleAdd = () => {
    handleShow();
    handleClear();
  };

  const handleEdit = (id) => {
    handleShow();
    axios.get(`http://localhost:5254/api/MedicalClaims/${id}`)
      .then((result) => {
        console.log('data', result);
        setPatientName(result.data.data.patientName);
        setDateOfService(result.data.data.dateOfService);
        setMedicalProvide(result.data.data.medicalProvider);
        setDiagnosis(result.data.data.diagnosis);
        setClaimAmount(result.data.data.claimAmount);
        setStatus(result.data.data.status);
      })
      .catch((error) => {
        console.log(error)
      })
  };

  const handleDelete = (id) => {
    if (window.confirm("Are you sure to delete this claim") == true) {
      alert(id);
    }
  };

  const handleUpdate = (id) => {
    handleShow();
  };

  const getData = () => {
    axios.get('http://localhost:5254/api/MedicalClaims')
      .then((result) => {
        console.log('data', result)
        setData(result.data.data)
      })
      .catch((error) => {
        console.log(error)
      })
  }

  const handleSave = () => {
    const url = 'http://localhost:5254/api/MedicalClaims'
    const data = {
      "patientName": patientName,
      "dateOfService": dateOfService,
      "medicalProvider": medicalProvide,
      "diagnosis": diagnosis,
      "claimAmount": claimAmount,
      "status": status
    }

    axios.post(url, data)
      .then((result) => {
        getData();
        handleClear();
        handleClose();
      })
  }

  return (
    <>
      <div>Claims</div>
      <Fragment>
        <Nav className="justify-content-end" activeKey="/home">
          <Nav.Item>
            <button
              className="btn btn-primary"
              onClick={() => handleAdd()}
            >
              Tambah
            </button>
          </Nav.Item>
        </Nav>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Patient Name</th>
              <th>Date Of Service</th>
              <th>Medical Provide</th>
              <th>Diagnosis</th>
              <th>Claim Amount</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data && data.length > 0
              ? data.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.patientName}</td>
                    <td>{item.dateOfService}</td>
                    <td>{item.medicalProvider}</td>
                    <td>{item.diagnosis}</td>
                    <td>{item.claimAmount}</td>
                    <td>{item.status}</td>
                    <td colSpan={2}>
                      <button
                        className="btn btn-primary"
                        onClick={() => handleEdit(item.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="btn btn-danger"
                        onClick={() => handleDelete(item.id)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              })
              : "Loading ..."}
          </tbody>
        </Table>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-lg">
              Form Medical Claims
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm={2}>
                Patient Name
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={patientName} onChange={(e) => setPatientName(e.target.value)} placeholder="Patient Name" />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm={2}>
                Date Of Service
              </Form.Label>
              <Col sm={10}>
                <Form.Control
                  type="date"
                  name="datepic"
                  placeholder="Date Of Service"
                  value={dateOfService}
                  onChange={(e) => setDateOfService(e.target.value)}
                />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm={2}>
                Medical Provide
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={medicalProvide} onChange={(e) => setMedicalProvide(e.target.value)} placeholder="Medical Provide" />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm={2}>
                Diagnosis
              </Form.Label>
              <Col sm={10}>
                <Form.Control as="textarea" value={diagnosis} onChange={(e) => setDiagnosis(e.target.value)} placeholder="Diagnosis" rows={3} />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm={2}>
                Amount
              </Form.Label>
              <Col sm={10}>
                <InputGroup className="mb-3">
                  <InputGroup.Text>Rp.</InputGroup.Text>
                  <Form.Control
                    type="number"
                    value={claimAmount}
                    onChange={(e) => setClaimAmount(e.target.value)}
                    aria-label="Amount (to the nearest dollar)"
                  />
                  <InputGroup.Text>.00</InputGroup.Text>
                </InputGroup>
              </Col>
            </Form.Group>

            <InputGroup className="mb-3">
              <Form.Label column sm={2}>
                Status
              </Form.Label>
              <Col sm={10}>

                <Form.Control
                  as="select"
                  value={status}
                  onChange={(event) => {
                    setStatus(event.target.value);
                  }}
                >
                  <option value="Pending">Pending</option>
                  <option value="Approved">Approved</option>
                  <option value="Rejected">Rejected</option>
                </Form.Control>
              </Col>
            </InputGroup>

          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={() => handleSave()}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    </>
  );
};

export default ClaimList;
